﻿using CAD.DSTableAdapters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CAD
{
    public class AgregarTipoDocumento
    {
        private static TipoDocumentoTableAdapter adaper = new TipoDocumentoTableAdapter();

        public static void InsertTipoDocumento(string Descripcion)
        {
            adaper.InsertTipoDocumento(Descripcion);
        }
    }
}
