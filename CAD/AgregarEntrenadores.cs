﻿using CAD.DSTableAdapters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CAD
{
    public class AgregarEntrenadores
    {
        private static EntrenadoresTableAdapter adapter = new EntrenadoresTableAdapter();
        private static string mensaje;

        public static string Mensaje { get => mensaje; set => mensaje = value; }

        public static bool InsertEntrenadores(int TipoDocumentoID,string Documento,string Nombre,string Apellidos,
                                              string Direccion,string Celular,string CorreoElectronico,DateTime FechaNacimiento)
        {
            try
            {
                adapter.InsertEntrenadores(TipoDocumentoID, Documento, Nombre, Apellidos, Direccion,Celular,CorreoElectronico,
                                            FechaNacimiento);
                mensaje = "Entredor ingresado con exito";
                return true;
            }
            catch (Exception ex)
            {

                return false;
            }
        }
    }
}
