﻿using CAD.DSTableAdapters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CAD
{
    public  class IngresarDeportista
    {
        private static DeportistaTableAdapter adapter = new DeportistaTableAdapter();

        private static string mensaje;

        public static string Mensaje { get => mensaje;  }

        public static bool InsertDeportista(int TipoDocumentoID,string Documento,string Nombre,string Apellidos,
                                           string Direccion,string Celular,string CorreoElectronico,DateTime FechaNacimiento)
        {
            try
            {
                adapter.InsertDeportista(TipoDocumentoID, Documento, Nombre, Apellidos, Direccion,
                                            Celular, CorreoElectronico, FechaNacimiento);
                mensaje = "Excelente se agrego";
                return true;
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("IX_Documento"))
                {
                    mensaje = " Ya existe documento de identidad";
                }
                else
                {
                    mensaje = ex.Message;
                }
                return false;
            }
        }


    }
}
