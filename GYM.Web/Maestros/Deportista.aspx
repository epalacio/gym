﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Deportista.aspx.cs" Inherits="GYM.Web.WebForm3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h1>Deportista</h1>
    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <table class="nav-justified" style="width: 60%">
        <tr>
            <td class="modal-sm" style="width: 133px; height: 28px;">Tipo Documento:</td>
            <td style="height: 28px; width: 287px;">
                <asp:DropDownList ID="tipoDocumentoDropDownList" runat="server" Height="18px" Width="246px" DataSourceID="TipoDocumentoSqlDataSource" DataTextField="Descripcion" DataValueField="TipoDocumentoID">
                </asp:DropDownList>
            </td>
            <td class="modal-sm" style="width: 196px; height: 28px;">Documento:</td>
            <td style="height: 28px">
                <asp:TextBox ID="DocumentoTextBox" runat="server" Style="margin-left: 0"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="modal-sm" style="width: 133px; height: 26px;">Nombre:</td>
            <td style="height: 26px; width: 287px;">
                <asp:TextBox ID="NombreTextBox" runat="server" Style="margin-left: 0"></asp:TextBox>
            </td>
            <td class="modal-sm" style="width: 196px; height: 26px;">Apellidos: </td>
            <td style="height: 26px">
                <asp:TextBox ID="ApellidosTextBox" runat="server" Style="margin-left: 0"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="modal-sm" style="width: 133px; height: 27px;">Dirección: </td>
            <td style="height: 27px; width: 287px;">
                <asp:TextBox ID="DireccionTextBox" runat="server" Style="margin-left: 0"></asp:TextBox>
            </td>
            <td class="modal-sm" style="width: 196px; height: 27px;">Celular: </td>
            <td style="height: 27px">
                <asp:TextBox ID="CelularTextBox" runat="server" Style="margin-left: 0"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="modal-sm" style="width: 133px">Email</td>
            <td class="modal-sm" style="width: 287px">
                <asp:TextBox ID="EmailTextBox" runat="server" Style="margin-left: 0"></asp:TextBox>
            </td>
            <td class="modal-sm" style="width: 196px">Fecha de nacimiento</td>
            <td>
                <asp:TextBox ID="FechaNacimientoTextBox" runat="server" Style="margin-left: 0"></asp:TextBox>
            </td>
        </tr>
    </table>

    <p>&nbsp;</p>
    <p>
        <asp:Button ID="DeportistaButton" runat="server" Text="Agregar Deportista" OnClick="DeportistaButton_Click" />
    </p>
    <p>&nbsp;</p>
    <p>
        <asp:Label ID="MensajeLabel" runat="server" Style="color: #0000CC; font-size: x-large"></asp:Label>
    </p>
    <p>
        &nbsp;
    </p>
    <p>
        <asp:GridView ID="DeportistaGridView" runat="server" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="DeportistaID" DataSourceID="DeportistaSqlDataSource" EmptyDataText="There are no data records to display." ForeColor="#333333" GridLines="None" Width="1228px">
            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            <Columns>
                <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
                <asp:BoundField DataField="DeportistaID" HeaderText="Deportista ID" ReadOnly="True" SortExpression="DeportistaID" />
                <asp:BoundField DataField="TipoDocumentoID" HeaderText="TipoDocumento ID" SortExpression="TipoDocumentoID" />
                <asp:BoundField DataField="Documento" HeaderText="Documento" SortExpression="Documento" />
                <asp:BoundField DataField="Nombre" HeaderText="Nombre" SortExpression="Nombre" />
                <asp:BoundField DataField="Apellidos" HeaderText="Apellidos" SortExpression="Apellidos" />
                <asp:BoundField DataField="Direccion" HeaderText="Dirección" SortExpression="Direccion" />
                <asp:BoundField DataField="Celular" HeaderText="Celular" SortExpression="Celular" />
                <asp:BoundField DataField="CorreoElectronico" HeaderText="Correo Electronico" SortExpression="CorreoElectronico" />
                <asp:BoundField DataField="FechaNacimiento" HeaderText="Fecha Nacimiento" SortExpression="FechaNacimiento" />
            </Columns>
            <EditRowStyle BackColor="#999999" />
            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
            <SortedAscendingCellStyle BackColor="#E9E7E2" />
            <SortedAscendingHeaderStyle BackColor="#506C8C" />
            <SortedDescendingCellStyle BackColor="#FFFDF8" />
            <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
        </asp:GridView>
        <asp:SqlDataSource ID="DeportistaSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" DeleteCommand="DELETE FROM [Deportista] WHERE [DeportistaID] = @DeportistaID" InsertCommand="INSERT INTO [Deportista] ([TipoDocumentoID], [Documento], [Nombre], [Apellidos], [Direccion], [Celular], [CorreoElectronico], [FechaNacimiento]) VALUES (@TipoDocumentoID, @Documento, @Nombre, @Apellidos, @Direccion, @Celular, @CorreoElectronico, @FechaNacimiento)" SelectCommand="SELECT [DeportistaID], [TipoDocumentoID], [Documento], [Nombre], [Apellidos], [Direccion], [Celular], [CorreoElectronico], [FechaNacimiento] FROM [Deportista]" UpdateCommand="UPDATE [Deportista] SET [TipoDocumentoID] = @TipoDocumentoID, [Documento] = @Documento, [Nombre] = @Nombre, [Apellidos] = @Apellidos, [Direccion] = @Direccion, [Celular] = @Celular, [CorreoElectronico] = @CorreoElectronico, [FechaNacimiento] = @FechaNacimiento WHERE [DeportistaID] = @DeportistaID">
            <DeleteParameters>
                <asp:Parameter Name="DeportistaID" Type="Int32" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="TipoDocumentoID" Type="Int32" />
                <asp:Parameter Name="Documento" Type="String" />
                <asp:Parameter Name="Nombre" Type="String" />
                <asp:Parameter Name="Apellidos" Type="String" />
                <asp:Parameter Name="Direccion" Type="String" />
                <asp:Parameter Name="Celular" Type="String" />
                <asp:Parameter Name="CorreoElectronico" Type="String" />
                <asp:Parameter DbType="Date" Name="FechaNacimiento" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="TipoDocumentoID" Type="Int32" />
                <asp:Parameter Name="Documento" Type="String" />
                <asp:Parameter Name="Nombre" Type="String" />
                <asp:Parameter Name="Apellidos" Type="String" />
                <asp:Parameter Name="Direccion" Type="String" />
                <asp:Parameter Name="Celular" Type="String" />
                <asp:Parameter Name="CorreoElectronico" Type="String" />
                <asp:Parameter DbType="Date" Name="FechaNacimiento" />
                <asp:Parameter Name="DeportistaID" Type="Int32" />
            </UpdateParameters>
        </asp:SqlDataSource>
        &nbsp;
        <asp:SqlDataSource ID="TipoDocumentoSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="select TipoDocumentoID, Descripcion from TipoDocumento
             union
             select 0,'[Seleccion tipo de documento]'
             order by 2"></asp:SqlDataSource>
    </p>
    <p>
        &nbsp;
    </p>

</asp:Content>
