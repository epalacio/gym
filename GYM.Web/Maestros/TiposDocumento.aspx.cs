﻿using CAD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GYM.Web
{
    public partial class WebForm2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void AgregarButton_Click(object sender, EventArgs e)
        {
            if (!ValidarCampos()) return;
            AgregarTipoDocumento.InsertTipoDocumento(DescripcioTextBox.Text);
            TipoDocumentoGridView.DataBind();
            MensajeLabel.Text = "Tipo documento ingresado con exito";

            DescripcioTextBox.Text = string.Empty;
            DescripcioTextBox.Focus();
            
        }

        private bool ValidarCampos()
        {
            if (DescripcioTextBox.Text == string.Empty)
            {
                MensajeLabel.Text = "Debes insertar una descripcion del documento";
                DescripcioTextBox.Focus();
                return false;
            }
      

            return true;
        }
    }
}