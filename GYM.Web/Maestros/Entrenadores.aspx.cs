﻿using CAD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GYM.Web
{
    public partial class WebForm4 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
          
        }

        protected void DeportistaButton_Click(object sender, EventArgs e)
        {
            if (!ValidarCampos()) return;
            if (!AgregarEntrenadores.InsertEntrenadores(Convert.ToInt32(TipoDocumentoDropDownList.SelectedValue),
                                                  DocumentoTextBox.Text,
                                                  NombreTextBox.Text,
                                                  ApellidosTextBox.Text,
                                                  DireccionTextBox.Text,
                                                  CelularTextBox.Text,
                                                  CorreoTextBox.Text,
                                                  Convert.ToDateTime(FechaNacimientoTextBox.Text)))
            {
                MensajeLabel.Text = AgregarEntrenadores.Mensaje;
                return;
            }
            MensajeLabel.Text = "Entrenador ingresado con exito";
            EntrenadorGridView.DataBind();

            TipoDocumentoDropDownList.SelectedIndex = 0;
            DocumentoTextBox.Text = string.Empty;
            NombreTextBox.Text = string.Empty;
            ApellidosTextBox.Text = string.Empty;
            DireccionTextBox.Text = string.Empty;
            CorreoTextBox.Text = string.Empty;
            FechaNacimientoTextBox.Text = string.Empty;
            TipoDocumentoDropDownList.Focus();
          
        }

        private bool ValidarCampos()
        {
            if (TipoDocumentoDropDownList.SelectedIndex == 0)
            {
                MensajeLabel.Text = " Debes selccionar un tipo de documento";
                TipoDocumentoDropDownList.Focus();
                return false;
            }

            if (DocumentoTextBox.Text == string.Empty)
            {
                MensajeLabel.Text = "Debes ingresar numero documento";
                DocumentoTextBox.Focus();
                return false;
            }

            if (NombreTextBox.Text == string.Empty)
            {
                MensajeLabel.Text = "Debes ingresar un nombre";
                NombreTextBox.Focus();
                return false;
            }

            if (ApellidosTextBox.Text == string.Empty)
            {
                MensajeLabel.Text = "Debes ingresar un apellido";
                ApellidosTextBox.Focus();
                return false;
            }

            if (DireccionTextBox.Text == string.Empty)
            {
                MensajeLabel.Text = "Debes ingresar una direccion";
                DireccionTextBox.Focus();
                return false;
            }

            if (CelularTextBox.Text == string.Empty)
            {
                MensajeLabel.Text = "Debes ingresar numero celular";
                CelularTextBox.Focus();
                return false;
            }

            if (CorreoTextBox.Text == string.Empty)
            {
                MensajeLabel.Text = "Debes ingresar un correo";
                CorreoTextBox.Focus();
                return false;
            }

            RegexUtilities miValidador = new RegexUtilities();
            if (!miValidador.IsValidEmail(CorreoTextBox.Text))
            {
                MensajeLabel.Text = "Correo no valido";
                CorreoTextBox.Focus();
                return false;
            }

            if (FechaNacimientoTextBox.Text == string.Empty)
            {
                MensajeLabel.Text = "Debes ingresar una fecha de nacimiento";
                FechaNacimientoTextBox.Focus();
                return false;
            }

            return true;
        }
    }
}