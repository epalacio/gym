﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="TiposDocumento.aspx.cs" Inherits="GYM.Web.WebForm2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h1>Tipo documento</h1>

    <p>&nbsp;</p>
    <table class="nav-justified" style="width: 25%">
        <tr>
            <td class="modal-sm" style="width: 167px; height: 32px">Descripción</td>
            <td style="height: 32px">
                <asp:TextBox ID="DescripcioTextBox" runat="server"></asp:TextBox>
            </td>
        </tr>
    </table>
  
    <p>&nbsp;</p>
    <p>
        <asp:Button ID="AgregarButton" runat="server" Text="Agregar tipo documento" OnClick="AgregarButton_Click" />
    </p>
      <p>&nbsp;</p>
    <p>
        <asp:Label ID="MensajeLabel" runat="server" Style="color: #0000CC; font-size: x-large"></asp:Label>
    </p>
    <p>
        &nbsp;</p>

    <p>
        <asp:GridView ID="TipoDocumentoGridView" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="TipoDocumentoID" DataSourceID="TipoDocumentoSqlDataSource">
            <Columns>
                <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
                <asp:BoundField DataField="TipoDocumentoID" HeaderText="TipoDocumentoID" InsertVisible="False" ReadOnly="True" SortExpression="TipoDocumentoID" />
                <asp:BoundField DataField="Descripcion" HeaderText="Descripcion" SortExpression="Descripcion" />
            </Columns>
        </asp:GridView>
        <asp:SqlDataSource ID="TipoDocumentoSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" DeleteCommand="DELETE FROM [TipoDocumento] WHERE [TipoDocumentoID] = @TipoDocumentoID" InsertCommand="INSERT INTO [TipoDocumento] ([Descripcion] ) VALUES (@Descipcion)" SelectCommand="SELECT [TipoDocumentoID], [Descripcion] FROM [TipoDocumento]" UpdateCommand="UPDATE [TipoDocumento] SET [Descripcion] = @Descripcion WHERE [TipoDocumentoID] = @TipoDocumentoID">
            <DeleteParameters>
                <asp:Parameter Name="TipoDocumentoID" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="Descipcion" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="Descripcion" />
                <asp:Parameter Name="TipoDocumentoID" />
            </UpdateParameters>
        </asp:SqlDataSource>
    </p>

</asp:Content>
