﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Productos.aspx.cs" Inherits="GYM.Web.WebForm1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h1>Productos</h1>
    <p>&nbsp;</p>
    <table class="nav-justified" style="width: 46%">
        <tr>
            <td class="modal-sm" style="width: 167px; height: 32px">Descripción</td>
            <td style="height: 32px">
                <asp:TextBox ID="DescripcioTextBox" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="modal-sm" style="width: 167px; height: 37px">Precio</td>
            <td style="height: 37px">
                <asp:TextBox ID="PrecioTextBox" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="modal-sm" style="width: 167px">Stock</td>
            <td>
                <asp:TextBox ID="StockTextBox" runat="server"></asp:TextBox>
            </td>
        </tr>
    </table>
    <p>&nbsp;</p>
    <p>
        <asp:Label ID="MensajeLabel" runat="server" style="color: #0000CC; font-size: x-large"></asp:Label>
    </p>
    <p>&nbsp;</p>
    <p>
        <asp:Button ID="AgregarButton" runat="server" Text="Agregar Productos" OnClick="AgregarButton_Click" Width="160px" />
    </p>
    <p>
        <asp:GridView ID="ProductosGridView" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="ProductoID" DataSourceID="ProductoSqlDataSource" EmptyDataText="There are no data records to display." ForeColor="#333333" GridLines="None" Width="509px">
            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            <Columns>
                <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
                <asp:BoundField DataField="ProductoID" HeaderText="Producto ID" ReadOnly="True" SortExpression="ProductoID" />
                <asp:BoundField DataField="Descripcion" HeaderText="Descripción" SortExpression="Descripcion" />
                <asp:BoundField DataField="Precio" DataFormatString="{0:C1}" HeaderText="Precio" SortExpression="Precio">
                    <ItemStyle HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="Stock" DataFormatString="{0:N1}" HeaderText="Stock" SortExpression="Stock">
                    <ItemStyle HorizontalAlign="Left" />
                </asp:BoundField>
            </Columns>
            <EditRowStyle BackColor="#999999" />
            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
            <SortedAscendingCellStyle BackColor="#E9E7E2" />
            <SortedAscendingHeaderStyle BackColor="#506C8C" />
            <SortedDescendingCellStyle BackColor="#FFFDF8" />
            <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
        </asp:GridView>
        <asp:SqlDataSource ID="ProductoSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" DeleteCommand="DELETE FROM [Producto] WHERE [ProductoID] = @ProductoID" InsertCommand="INSERT INTO [Producto] ([Descripcion], [Precio], [Stock]) VALUES (@Descripcion, @Precio, @Stock)" SelectCommand="SELECT [ProductoID], [Descripcion], [Precio], [Stock] FROM [Producto]" UpdateCommand="UPDATE [Producto] SET [Descripcion] = @Descripcion, [Precio] = @Precio, [Stock] = @Stock WHERE [ProductoID] = @ProductoID">
            <DeleteParameters>
                <asp:Parameter Name="ProductoID" Type="Int32" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="Descripcion" Type="String" />
                <asp:Parameter Name="Precio" Type="Decimal" />
                <asp:Parameter Name="Stock" Type="Double" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="Descripcion" Type="String" />
                <asp:Parameter Name="Precio" Type="Decimal" />
                <asp:Parameter Name="Stock" Type="Double" />
                <asp:Parameter Name="ProductoID" Type="Int32" />
            </UpdateParameters>
        </asp:SqlDataSource>
    </p>
    <p>
        &nbsp;
    </p>


</asp:Content>
