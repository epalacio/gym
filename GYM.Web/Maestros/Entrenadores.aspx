﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Entrenadores.aspx.cs" Inherits="GYM.Web.WebForm4" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h1>Entrenadores</h1>
    <p>&nbsp;</p>
    <table class="nav-justified" style="width: 70%">
        <tr>
            <td class="modal-sm" style="width: 133px; height: 28px;">Tipo Documento:</td>
            <td style="height: 28px; width: 287px;">
                <asp:DropDownList ID="TipoDocumentoDropDownList" runat="server" DataSourceID="TipoDocumentoSqlDataSource" DataTextField="Descripcion" DataValueField="TipoDocumentoID" Height="24px" Width="208px">
                </asp:DropDownList>
            </td>
            <td class="modal-sm" style="width: 196px; height: 28px;">Documento:</td>
            <td style="height: 28px">
                <asp:TextBox ID="DocumentoTextBox" runat="server" Style="margin-left: 0"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="modal-sm" style="width: 133px; height: 26px;">Nombre:</td>
            <td style="height: 26px; width: 287px;">
                <asp:TextBox ID="NombreTextBox" runat="server" Style="margin-left: 0"></asp:TextBox>
            </td>
            <td class="modal-sm" style="width: 196px; height: 26px;">Apellidos: </td>
            <td style="height: 26px">
                <asp:TextBox ID="ApellidosTextBox" runat="server" Style="margin-left: 0"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="modal-sm" style="width: 133px; height: 27px;">Dirección: </td>
            <td style="height: 27px; width: 287px;">
                <asp:TextBox ID="DireccionTextBox" runat="server" Style="margin-left: 0"></asp:TextBox>
            </td>
            <td class="modal-sm" style="width: 196px; height: 27px;">Celular: </td>
            <td style="height: 27px">
                <asp:TextBox ID="CelularTextBox" runat="server" Style="margin-left: 0"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="modal-sm" style="width: 133px">Correo</td>
            <td class="modal-sm" style="width: 287px">
                <asp:TextBox ID="CorreoTextBox" runat="server" Style="margin-left: 0"></asp:TextBox>
            </td>
            <td class="modal-sm" style="width: 196px">Fecha de nacimiento</td>
            <td>
                <asp:TextBox ID="FechaNacimientoTextBox" runat="server" Style="margin-left: 0"></asp:TextBox>
            </td>
        </tr>
    </table>
        <p>&nbsp;</p>
    <p>
        <asp:Button ID="DeportistaButton" runat="server" Text="Agregar entrenador" OnClick="DeportistaButton_Click" />
    </p>
    <p>
        <asp:Label ID="MensajeLabel" runat="server" Style="color: #0000CC; font-size: x-large"></asp:Label>
        <asp:GridView ID="EntrenadorGridView" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="EntrenadoID" DataSourceID="entrenadoresSqlDataSource" ForeColor="#333333" GridLines="None" Width="1177px">
            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            <Columns>
                <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" ShowSelectButton="True" />
                <asp:BoundField DataField="EntrenadoID" HeaderText="EntrenadoID" InsertVisible="False" ReadOnly="True" SortExpression="EntrenadoID" />
                <asp:BoundField DataField="TipoDocumentoID" HeaderText="TipoDocumentoID" SortExpression="TipoDocumentoID" />
                <asp:BoundField DataField="Nombre" HeaderText="Nombre" SortExpression="Nombre" />
                <asp:BoundField DataField="Documento" HeaderText="Documento" SortExpression="Documento" />
                <asp:BoundField DataField="Apellidos" HeaderText="Apellidos" SortExpression="Apellidos" />
                <asp:BoundField DataField="Direccion" HeaderText="Direccion" SortExpression="Direccion" />
                <asp:BoundField DataField="Celular" HeaderText="Celular" SortExpression="Celular" />
                <asp:BoundField DataField="CorreoElectronico" HeaderText="CorreoElectronico" SortExpression="CorreoElectronico" />
                <asp:BoundField DataField="FechaNacimiento" HeaderText="FechaNacimiento" SortExpression="FechaNacimiento" />
            </Columns>
            <EditRowStyle BackColor="#999999" />
            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
            <SortedAscendingCellStyle BackColor="#E9E7E2" />
            <SortedAscendingHeaderStyle BackColor="#506C8C" />
            <SortedDescendingCellStyle BackColor="#FFFDF8" />
            <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
        </asp:GridView>
    </p>
    <asp:SqlDataSource ID="entrenadoresSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" DeleteCommand="DELETE FROM [Entrenadores] WHERE [EntrenadoID] = @EntrenadoID" InsertCommand="INSERT INTO [Entrenadores] ([TipoDocumentoID], [Documento], [Nombre], [Apellidos], [Direccion], [Celular], [CorreoElectronico], [FechaNacimiento]) VALUES (@TipoDocumentoID, @Documento, @Nombre, @Apellidos, @Direccion, @Celular, @CorreoElectronico, @FechaNacimiento)
" SelectCommand="SELECT [EntrenadoID], [TipoDocumentoID], [Nombre], [Documento], [Apellidos], [Direccion], [Celular], [CorreoElectronico], [FechaNacimiento] FROM [Entrenadores]" UpdateCommand="UPDATE [Entrenadores] SET [TipoDocumentoID] = @TipoDocumentoID, [Documento] = @Documento, [Nombre] = @Nombre, [Apellidos] = @Apellidos, [Direccion] = @Direccion, [Celular] = @Celular, [CorreoElectronico] = @CorreoElectronico, [FechaNacimiento] = @FechaNacimiento WHERE [EntrenadoID] = @EntrenadoID
">
        <DeleteParameters>
            <asp:Parameter Name="EntrenadoID" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="TipoDocumentoID" />
            <asp:Parameter Name="Documento" />
            <asp:Parameter Name="Nombre" />
            <asp:Parameter Name="Apellidos" />
            <asp:Parameter Name="Direccion" />
            <asp:Parameter Name="Celular" />
            <asp:Parameter Name="CorreoElectronico" />
            <asp:Parameter Name="FechaNacimiento" />
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="TipoDocumentoID" />
            <asp:Parameter Name="Documento" />
            <asp:Parameter Name="Nombre" />
            <asp:Parameter Name="Apellidos" />
            <asp:Parameter Name="Direccion" />
            <asp:Parameter Name="Celular" />
            <asp:Parameter Name="CorreoElectronico" />
            <asp:Parameter Name="FechaNacimiento" />
            <asp:Parameter Name="EntrenadoID" />
        </UpdateParameters>
    </asp:SqlDataSource>
    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <p>
        <asp:SqlDataSource ID="TipoDocumentoSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="select TipoDocumentoID, Descripcion from TipoDocumento
union
select 0,'[Seleccion tipo de documento]'
order by 2"></asp:SqlDataSource>
    </p>
    <p>&nbsp;</p>


    


</asp:Content>
