﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="GYM.Web._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron">
        <div class="col-md-4">
            <h1>ASP.NET</h1>
        </div>
        <div class="col-md-4">
        </div>
        <div class="col-md-4">
            <asp:Image ID="Image3" runat="server" Height="124px" ImageUrl="~/Image/logp.png" Width="266px" />
        </div>
        <p><a href="" class="btn btn-primary btn-lg">Learn more &raquo;</a></p>
    </div>

    <table>
        <tr>
            <td>
                <div class="row">
                    <div class="col-md-4">
                        <h2>Maestros</h2>
                        <p>
                            <asp:LinkButton ID="ProductosLinkButton" runat="server" PostBackUrl="~/Maestros/Productos.aspx" CssClass="btn btn-primary">Productos</asp:LinkButton><br />
                            <asp:LinkButton ID="DeportistaLinkButton" runat="server" PostBackUrl="~/Maestros/Deportista.aspx" CssClass="btn btn-primary">Deportista</asp:LinkButton><br />
                            <asp:LinkButton ID="TipoDocumentoLinkButton" runat="server" PostBackUrl="~/Maestros/TiposDocumento.aspx" CssClass="btn btn-primary">Tipo Documento</asp:LinkButton><br />
                            <asp:LinkButton ID="EntrenadorLinkButton" runat="server" PostBackUrl="~/Maestros/Entrenadores.aspx" CssClass="btn btn-primary">Entrenador</asp:LinkButton><br />
                            <asp:LinkButton ID="EspacioLinkButton" runat="server" PostBackUrl="~/Maestros/EspacioDeportivo.aspx" CssClass="btn btn-primary">Esapcio deportivo</asp:LinkButton>
                        </p>
                    
                    </div>
                    <div class="col-md-4">
                        <h2>Movimientos</h2>
                        <p>
                            projects.
                        </p>
                        <p>
                            <a class="btn btn-default" href="">Learn more &raquo;</a>
                        </p>
                    </div>
                    <div class="col-md-4">
                        <h2>Reportes</h2>
                        <p>
                            plications.
                        </p>
                        <p>
                            <a class="btn btn-default" href="">Learn more &raquo;</a>
                        </p>
                    </div>
                </div>
            </td>
            <td>
                <asp:Image ID="Image1" runat="server" Height="124px" ImageUrl="~/Image/logp.png" Width="266px" />
            </td>
        </tr>
    </table>
</asp:Content>
